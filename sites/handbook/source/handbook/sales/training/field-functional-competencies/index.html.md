---
layout: handbook-page-toc
title: "Field Functional Competencies"
description: "GitLab Field functional competencies define the critical skills, behaviors, and attitudes that GitLab Field team members must demonstrate to successfully deliver desired outcomes. They provide a common language and consistent expectations for what good looks like for the GitLab Field organization."
---

# Field Functional Competencies  
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# What are Field Functional Competencies? 

GitLab Field functional competencies provide a common language and consistent expectations for “what good looks like” for the GitLab Field organization (inclusive of Sales and Customer Success roles). They are intended to complement [GitLab’s values](/handbook/values/) and are comprised of specific behaviors that GitLab team members in a field role need to demonstrate consistently to be successful. These critical field skills and behaviors are not meant to be role or job-specific (as each role or job may have additional unique requirements), and how each competency is expressed or demonstrated may vary by role (e.g. by technical depth, engagement in different stages of the customer journey, team-specific expectations, etc.). 

More specifically, functional competencies help to:
*  Guide the growth and development opportunities available for GitLab Field team members
*  Build organizational capabilities when adopted and practiced across the Field organization
*  Reinforce behavior that is consistent with GitLab’s values and culture
*  Enable GitLab Field team members to build productive relationships and generate positive results

There are three primary competencies for the Field: Customer Focus, Solution Focus, and Operational Excellence. Please refer to the tables below to learn more. 

## Customer Focus

Field team members should be focused on the customer and delivering a world class experience with every interaction. 

| EXAMPLE SKILLS / BEHAVIORS | DESCRIPTION |
| ---- | ----- |
| Understanding the Customer's Needs | Leverages active listening to create mutual understanding between GitLab Team and customer regarding their short-term and long-term business challenges. Keeps the customer and their goals in mind when engaging with them. Seeks to understand their Positive Business Outcomes and how they define and measure success. |
| Customer Business Acumen | Understands the dynamics, trends, and challenges of the customer's business and their industry then appropriately advocates internally for the needs of the customer. | 
| Effective Communication | Shares knowledge with customers in a way that creates credibility, demonstrates integrity, and conveys GitLab values. Adjusts presentations and delivery based on audience level and interests/needs. Communicates crisply and concisely (whether in person or remotely) and can capture and engage audiences with effective storytelling. |
| Facilitating (Customer Presence) | Projects confidence, credibility, and conviction in body language, voice, and words during meetings to show interest, gain respect, and inspire trust. Drives effective bi-directional dialogue that lead to productive outcomes for the customer and for GitLab. |

## Solution Focus

Field team members should be focused on how GitLab helps customers solve their business problems and accelerate their success.

| EXAMPLE SKILLS / BEHAVIORS | DESCRIPTION |
| ---- | ----- |
| Consultative Approach | Uses Command of the Message and MEDDPPICC to effectively explore and diagnose customer needs and recommend appropriate value-based solutions to help customers achieve their strategic business goals. |
| Leading with Vision | Develops, documents, and executes effective strategies to land new customers with vision and purposefully expand existing customers adoption of GitLab technologies to accelerate customers' time to value and return on investment as they progress in their DevOps adoption journey. |
| DevOps and Technical Leadership | Demonstrates a comprehensive understanding of how products and services integrate to form solutions in the context of the customer's ecosystem. Advises customers on the importance of aligning people, process, and technology to achieve desired outcomes via an Agile DevOps methodology. |
| Building Trust (Trusted Advisor) | Connects with key stakeholders on both a personal and professional level and shows up as a credible and reliable champion for customers. Effectively positions GitLab as a partner to the customer rather than just another vendor with a focus on following through on commitments and delivering results. |

## Operational Excellence
 
To drive efficiency, Field team members should adhere to the tools and processes established by the Field Operations team and their leadership.

| EXAMPLE SKILLS / BEHAVIORS | DESCRIPTION |
| ---- | ----- |
| Process Discipline | Leverages repeatable and scalable processes and tools throughout the customer lifecycle. Effectively manages time, priorities, and daily workload to effectively engage customers. |
| Growth Mindset | Demonstrates the curiosity, perseverance, and willingness to take calculated risks to continuously pursue opportunities to improve knowledge, skills, processes, and more that lead to improved results. | 
| Account Planning & Management | Creates and implements effective long-term account expansion strategies by leveraging standard processes, tools, and content (including customer use cases and sales plays) |
| Resource Orchestration | Engages partners and internal resources and leverages tools at the right time and in the right manner to move the customer forward while prioritizing time and resources. |

# Proficiency Levels

Competency proficiency rating levels align with GitLab’s enterprise-wide performance categories:
- [Developing](/handbook/people-group/performance-assessments-and-succession-planning/#developing)
- [Performing](/handbook/people-group/performance-assessments-and-succession-planning/#performing)
- [Exceeding](/handbook/people-group/performance-assessments-and-succession-planning/#exceeding) 

# Practical Application

Field team members and their managers are encouraged to have regular coaching and development conversations to assess individual and team strengths and areas of opportunity across all competencies and the supporting/example skills and behaviors. More specifically, managers in the Field organization should:
1. Encourage team members to complete their own self-assessment (see template below) to
    - Identify 2 or more strengths Field team members intend to continue to leverage to deliver positive results
    - Identify 2 specific skills / behaviors Field team members would like to improve upon
1. In parallel, managers should complete a similar assessment (see template below) for each of his or her direct reports
1. Field team members and their managers should then meet to compare and discuss assessment results then collaboratively brainstorm opportunities and agree on specific plans to support the team member's professional development and ongoing improvement
1. Team members are encouraged to identify and regularly engage with an accountability partner (whether that be their manager and/or other) to maintain focus on development plans and to provide ongoing feedback

## Self-Assessment 

On an annual basis the Field Enablement Team will launch a self- assessment for Field team members with CultureAmp. This information will help the Field Enablement Team prioritize training needs and assess the effacacy of training programs throughout the year. This will not be used for performance reviews. 
